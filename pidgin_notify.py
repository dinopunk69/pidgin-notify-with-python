#!/usr/bin/env python

def cb (Notification=None, action=None, Data=None, sender=None):
    print "sender = " + sender
    pass

def notify_me(account, sender, message, conversation, flags):
    if bus.pidginbus.PurpleConversationHasFocus(conversation) == 0:
        
        buddy = bus.pidginbus.PurpleFindBuddy(account,str(sender.split("@")[0]))
        name = bus.pidginbus.PurpleBuddyGetAlias(buddy)
        #icon = bus.pidginbus.PurpleBuddyGetIcon(buddy)
        #icon_path = bus.pidginbus.PurpleBuddyIconGetFullPath(icon)
        conv = str(conversation)
        
        msg = message.replace("\n"," ")
        if re.search(r'packer|robert|transcod|preview|robbie|rob', msg, re.I):
            msg = "\""+msg
            if len(msg) > 32:
                msg = msg[:32]+"...\""
            
            #print name, "("+sender+") said \""+message+"\""
            
            direction = "Incoming"
            if flags != 2:
                direction = "Outgoing"
            # just print out some stuff
            print "Account/ConvId(%s/%s): %s message - %s said: \"%s\"" % (account, conversation, direction, sender, message)
        
            # Set the notify message
            pynotify.init(os.path.splitext(os.path.basename(sys.argv[0]))[0])
            n = pynotify.Notification(name+" ("+sender+")",msg+" ","pidgin")
            n.set_hint_string("dbus-callback-default","im.pidgin.purple.PurpleService /im/pidgin/purple/PurpleObject im.pidgin.purple.PurpleInterface purple_conversation_present int32:"+conv)
            n.show()
            
            my_account_id = bus.pidginbus.PurpleAccountsGetAllActive()[0] # or some other account from yours
            #print "my_account_id = %s" % (my_account_id)
            #conv = bus.pidginbus.PurpleConversationNew(1, my_account_id, "rpacker@tropical.xmpp.slack.com")
            #conv = bus.pidginbus.PurpleConversationNew(1, my_account_id, "random@conference.tropical.xmpp.slack.com")
            #conv = bus.pidginbus.PurpleConversationNew(2, my_account_id, "random@conference.tropical.xmpp.slack.com")# (PURPLE_CONV_TYPE_IM, account, buddy_name))
            
            # This is just some random code I found that doesn't do shit
            #for p in bus.pidginbus.PurpleGetConversations():
            #    if bus.pidginbus.PurpleConversationGetName(p).find("random"):
            #        print p
            #        chat = p
            #        chat_data = bus.pidginbus.PurpleConversationGetChatData(chat)
            #        #bus.pidginbus.PurpleConvChatSetTopic(chat_data, user, topic)
            #        print chat_data
            #        bus.pidginbus.PurpleConvChatSend(chat_data, 'random@conference.tropical.xmpp.slack.com', '...', 0, int(time.time()))
            
            # This will send a message to chat, BUT for some reason it sent a LOT of them, like in a loop!
            #conv_im = bus.pidginbus.PurpleConvChat(conversation)
            #bus.pidginbus.PurpleConvChatSend(conv_im, "...")
            return True
        else:
            return False

import os
import sys
import gobject, dbus
import pynotify
import time
import re

from dbus.mainloop.glib import DBusGMainLoop

dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
bus = dbus.SessionBus()
obj = bus.get_object("im.pidgin.purple.PurpleService", "/im/pidgin/purple/PurpleObject")
bus.pidginbus = dbus.Interface(obj, "im.pidgin.purple.PurpleInterface")

bus.add_signal_receiver(notify_me,
                        dbus_interface="im.pidgin.purple.PurpleInterface",
                        signal_name="ReceivingChatMsg")


# This will print everybody on your slack list (buddies)
#for account in bus.pidginbus.PurpleAccountsGetAllActive():
#    for buddy in bus.pidginbus.PurpleFindBuddies(account, ''):
#        print bus.pidginbus.PurpleBuddyGetName(buddy)

# This will print all conversations (open IM windows and chats opened or not)
#for p in bus.pidginbus.PurpleGetConversations():
#    n = bus.pidginbus.PurpleConversationGetName(p)
#    print n

loop = gobject.MainLoop()
loop.run()

